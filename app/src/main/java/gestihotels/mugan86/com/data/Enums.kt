package gestihotels.mugan86.com.data

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 24/7/18.
 ********************************************************************************/
enum class ActivityReferences { ENTRYREGISTER, MAINACTIVITY, ENTRYVIEWS, SCANNER, PICNICCHECKS }