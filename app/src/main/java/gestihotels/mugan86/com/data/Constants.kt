package gestihotels.mugan86.com.data

import gestihotels.mugan86.com.R

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 24/7/18.
 ********************************************************************************/
class Constants {

    companion object{
        const val BASE_URL = "https://anartz-mugika.com/gesti-hotels/web/"
        //
        // const val BASE_URL = "http://192.168.1.5/gesti-hotels-backend/web/"
        const val BAR_CODE = "BarCode"
        const val BAR_CODE_NOT_FOUND = "Bar code not found"
        const val ERROR = "Error"
        const val HANDLER = "handler"
        const val PRESS_AGAIN_TO_SCAN = "Press again button to scan code"
        const val SCAN_ERROR_SOUND = R.raw.computer_error
        const val SCAN_OK_SOUND = R.raw.beep_normal
        const val STOP_CAMERA = "************** Stop Camera**********"

        val TOKEN = "d1a9356cbed83ecd0215afa478610c38"

        //PREFERENCES
        val USE_INTERNET = "USE_INTERNET"

        val ZXING_CAMERA_PERMISSION = 1
    }
}