package gestihotels.mugan86.com.data.server

import android.content.Context
import android.net.Uri
import com.google.gson.Gson
import gestihotels.mugan86.com.R
import gestihotels.mugan86.com.data.Constants
import gestihotels.mugan86.com.domain.model.Food
import gestihotels.mugan86.com.domain.model.Log
import gestihotels.mugan86.com.domain.model.Stay
import gestihotels.mugan86.com.extensions.isNetworkConnected
import gestihotels.mugan86.com.utils.Request

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 2/8/18.
 ********************************************************************************/
class EntryRegisterRequest(private val stay: Stay, private val food: Food, private val when_: String,
                           private val count: Int, private val picnic: Int = 0, private val access: String) {

    fun execute(context: Context) : Log? {
        if (isNetworkConnected(context)) {
            var manual = "1"
            if (access == context.resources.getString(R.string.access_automatic)) {
                manual = "0"
            }
            val builder = Uri.Builder()
                    .appendQueryParameter("token", Constants.TOKEN)
                    .appendQueryParameter("when", this.when_)
                    .appendQueryParameter("stay", this.stay.id.toString())
                    .appendQueryParameter("food", this.food.id.toString())
                    .appendQueryParameter("count", this.count.toString())
                    .appendQueryParameter("manual", manual)
            println(this.when_)
            println(this.stay.id.toString())
            println(this.food.id.toString())
            println(this.count.toString())
                    // .appendQueryParameter("picnic", this.picnic.toString())
            return Gson().fromJson(Request().sendPost("${Constants.BASE_URL}api/food/register/entry", builder), Log::class.java)
        }
        //Without Internet or / and qrCode is empty
        return Log()
    }
}