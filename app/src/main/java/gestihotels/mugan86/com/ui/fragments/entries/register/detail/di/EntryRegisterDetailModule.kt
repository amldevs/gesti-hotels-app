package gestihotels.mugan86.com.ui.fragments.entries.register.detail.di

import dagger.Module
import dagger.Provides
import gestihotels.mugan86.com.ui.fragments.entries.register.detail.EntryRegisterDetailFragment
import gestihotels.mugan86.com.ui.fragments.entries.register.detail.EntryRegisterDetailPresenter

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 27/7/18.
 ********************************************************************************/
@Module
class EntryRegisterDetailModule(val activity: EntryRegisterDetailFragment) {
    @Provides
    fun provideEntryRegisterDetailPresenter() = EntryRegisterDetailPresenter(activity)
}