package gestihotels.mugan86.com.domain.model

import android.os.Parcel
import android.os.Parcelable
import gestihotels.mugan86.com.utils.DateTime

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 29/7/18.
 ********************************************************************************/

// OK
data class User(val id: Int = -1, val name: String? = "", val lastname: String? = "", val username: String? = "",
                val token: String? = "", val role: String? = "ROLE_NOTHING"): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(lastname)
        parcel.writeString(username)
        parcel.writeString(token)
        parcel.writeString(role)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}

// OK
data class Client(val id: Int = -1, val id_client: Int? = null, val name: String? = "", val language: Language?): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readParcelable(Language::class.java.classLoader))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        if (id_client != null) {
            parcel.writeInt(id_client)
        }
        parcel.writeString(name)
        parcel.writeParcelable(language, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Client> {
        override fun createFromParcel(parcel: Parcel): Client {
            return Client(parcel)
        }

        override fun newArray(size: Int): Array<Client?> {
            return arrayOfNulls(size)
        }
    }
}

// OK
data class Language(val id: Int = -1, val code: String? = "es", val text: String? = "Español", val extra: String? = ""): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(code)
        parcel.writeString(text)
        parcel.writeString(extra)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Language> {
        override fun createFromParcel(parcel: Parcel): Language {
            return Language(parcel)
        }

        override fun newArray(size: Int): Array<Language?> {
            return arrayOfNulls(size)
        }
    }
}

data class NutritionalRegimen(val id: Int = -1, val code: String? = "",
                              val desc_es: String? = "", val desc_eu: String? = "", val desc_en: String? = "",
                              val desc_ca: String? = "", val active: Int = 1): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(code)
        parcel.writeString(desc_es)
        parcel.writeString(desc_eu)
        parcel.writeString(desc_en)
        parcel.writeString(desc_ca)
        parcel.writeInt(active)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NutritionalRegimen> {
        override fun createFromParcel(parcel: Parcel): NutritionalRegimen {
            return NutritionalRegimen(parcel)
        }

        override fun newArray(size: Int): Array<NutritionalRegimen?> {
            return arrayOfNulls(size)
        }
    }
}

data class FoodShift(val id: Int = -1, val code: String? = "out_food_shift", val name: String? = "", val start: String? = "",
                     val finish: String? = ""): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(code)
        parcel.writeString(name)
        parcel.writeString(start)
        parcel.writeString(finish)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FoodShift> {
        override fun createFromParcel(parcel: Parcel): FoodShift {
            return FoodShift(parcel)
        }

        override fun newArray(size: Int): Array<FoodShift?> {
            return arrayOfNulls(size)
        }
    }
}

data class Food(val id: Int = -1, val data: String? = "", val type: FoodShift? = FoodShift(), val created_by: User? = User()): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readParcelable(FoodShift::class.java.classLoader),
            parcel.readParcelable(User::class.java.classLoader))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(data)
        parcel.writeParcelable(type, flags)
        parcel.writeParcelable(created_by, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Food> {
        override fun createFromParcel(parcel: Parcel): Food {
            return Food(parcel)
        }

        override fun newArray(size: Int): Array<Food?> {
            return arrayOfNulls(size)
        }
    }
}

data class Room(val id: Int = -1, val number: Int = -1, val qr_code: String? = "",
                val hotel: Hotel? = Hotel()): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readParcelable(Hotel::class.java.classLoader))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(number)
        parcel.writeString(qr_code)
        parcel.writeParcelable(hotel, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Room> {
        override fun createFromParcel(parcel: Parcel): Room {
            return Room(parcel)
        }

        override fun newArray(size: Int): Array<Room?> {
            return arrayOfNulls(size)
        }
    }
}


data class Hotel (val id: Int = -1, val code: String? = "", val name: String? = "",
                  val stars: Int = -1, val rooms: Int = -1): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(code)
        parcel.writeString(name)
        parcel.writeInt(stars)
        parcel.writeInt(rooms)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Hotel> {
        override fun createFromParcel(parcel: Parcel): Hotel {
            return Hotel(parcel)
        }

        override fun newArray(size: Int): Array<Hotel?> {
            return arrayOfNulls(size)
        }
    }
}

data class Stay(val id: Int = -1, val count: Int = -1, val reservation_number: Int? = null, val start_data: String? = "", val finish_data: String? = "",
                val entry_in: FoodShift? = FoodShift(), val exit_in: FoodShift? = FoodShift(),
                val client: Client? = null, val nutritional_regimen: NutritionalRegimen? = NutritionalRegimen(),
                val room: Int? = null, val active: Int? = 0): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(FoodShift::class.java.classLoader),
            parcel.readParcelable(FoodShift::class.java.classLoader),
            parcel.readParcelable(Client::class.java.classLoader),
            parcel.readParcelable(NutritionalRegimen::class.java.classLoader),
            parcel.readInt(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(count)
        if (reservation_number != null) {
            parcel.writeInt(reservation_number)
        }
        parcel.writeString(start_data)
        parcel.writeString(finish_data)
        parcel.writeParcelable(entry_in, flags)
        parcel.writeParcelable(exit_in, flags)
        parcel.writeParcelable(client, flags)
        parcel.writeParcelable(nutritional_regimen, flags)
        if (room != null) {
            parcel.writeInt(room)
        }
        if (active != null) {
            parcel.writeInt(active)
        }
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Stay> {
        override fun createFromParcel(parcel: Parcel): Stay {
            return Stay(parcel)
        }

        override fun newArray(size: Int): Array<Stay?> {
            return arrayOfNulls(size)
        }
    }
}

data class Log (val code: Int = 500, val log: String? = "",
                val error: Int = -1, val errorLog: String = "",
                val moreEntries: Int = -1, val stayCount: Int? = 0, val stayEntryPeople: Int? = 0,
                val registerAvailable: Int? = 0, val entryCount: Int? = 0)

data class LogPicnicAction (val code: Int = 500, val log: String? = "")

data class ControlRestaurantList(val page: Int, val results: Int, val totalPages: Int, val shift: Int = -1,
                                 val items: ArrayList<ControlRestaurant>, val log: String?, val errorLog: Int = -1,
                                 val error: Int = -1): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readArrayList(null) as ArrayList<ControlRestaurant>,
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(page)
        parcel.writeInt(results)
        parcel.writeInt(totalPages)
        parcel.writeInt(shift)
        parcel.writeList(items)
        parcel.writeString(log)
        parcel.writeInt(errorLog)
        parcel.writeInt(error)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ControlRestaurantList> {
        override fun createFromParcel(parcel: Parcel): ControlRestaurantList {
            return ControlRestaurantList(parcel)
        }

        override fun newArray(size: Int): Array<ControlRestaurantList?> {
            return arrayOfNulls(size)
        }
    }
}

data class ControlRestaurant(val id: Int, val createdAt: String? = DateTime.currentDataTime, val picnic: Int = 0,
                             val peopleEntry: Int, val stayId: Int, val stayCountPeople: Int, val client: String?): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(createdAt)
        parcel.writeInt(picnic)
        parcel.writeInt(peopleEntry)
        parcel.writeInt(stayId)
        parcel.writeInt(stayCountPeople)
        parcel.writeString(client)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ControlRestaurant> {
        override fun createFromParcel(parcel: Parcel): ControlRestaurant {
            return ControlRestaurant(parcel)
        }

        override fun newArray(size: Int): Array<ControlRestaurant?> {
            return arrayOfNulls(size)
        }
    }
}

data class Picnic(val id: Int = -1, val serviceName: String = "", val code: String = "", val clientName: String ="", val room: Int=-1,
                  val total: Int=0, val observations: String = "", val orderTime: String="", val pickUpTime: String="", val active: Int=0)