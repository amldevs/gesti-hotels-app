package gestihotels.mugan86.com.ui.fragments.entries.register.detail

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import gestihotels.mugan86.com.App
import gestihotels.mugan86.com.R
import gestihotels.mugan86.com.domain.model.FoodShift
import gestihotels.mugan86.com.domain.model.Stay
import gestihotels.mugan86.com.extensions.afterTextChanged
import gestihotels.mugan86.com.extensions.hide
import gestihotels.mugan86.com.extensions.show
import gestihotels.mugan86.com.ui.activities.entries.register.EntryRegisterActivity
import gestihotels.mugan86.com.ui.fragments.entries.register.detail.di.EntryRegisterDetailModule
import kotlinx.android.synthetic.main.fragment_entry_register_detail.*
import javax.inject.Inject


/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 26/7/18.
 ********************************************************************************/
class EntryRegisterDetailFragment: Fragment(), EntryRegisterDetailPresenter.View {

    @Inject
    lateinit var presenterEntryRegister: EntryRegisterDetailPresenter
    val component by lazy { App().component.plus(EntryRegisterDetailModule(this)) }
    lateinit var stay: Stay
    lateinit var foodshift: FoodShift
    var access: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)
        presenterEntryRegister.onCreate((activity as EntryRegisterActivity))
        val b = arguments

        if (b != null) {
            stay = b.getParcelable("stay") as Stay
            access = b.getString("access") == "Manual"
        }

        foodshift = presenterEntryRegister.getThisMomentFoodShift()


    }
    // The onCreateView method is called when Fragment should create its View object hierarchy,
    // either dynamically or via XML layout inflation.
    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.fragment_entry_register_detail, parent, false)
    }

    private fun visualizeRegisterOptions(message: String, visible: Int) {
        infoMessagesTextView.text = message
        infoMessagesTextView.visibility = View.GONE
        entryRegisterOptionsLinearLayout.visibility = visible
        if (message == "") {
            clientEntryDataLinearLayout.visibility = View.VISIBLE
        } else {
            infoMessagesTextView.visibility = View.VISIBLE
            accessAutomaticSection.visibility = View.GONE
            clientEntryDataLinearLayout.visibility = View.GONE
            unblockToRegisterButton.visibility = View.VISIBLE
        }
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        if (this.stay != Stay()) {
            clientNameLastNameTextView.text = String.format("%s", this.stay.client!!.name)
            entryDataTextView.text = String.format("ENTRADA: %s - %s", this.stay.start_data, this.stay.entry_in!!.name)
            finishDataTextView.text = String.format("SALIDA: %s - %s", this.stay.finish_data, this.stay.exit_in!!.name)

            when {
                this.stay.nutritional_regimen!!.code == "PC" -> {
                    accessFoodsTextView.text = resources.getString(R.string.access_to_pc)
                    this.visualizeRegisterOptions("", View.VISIBLE)
                }
                this.stay.nutritional_regimen!!.code == "MP" -> {
                    accessFoodsTextView.text = resources.getString(R.string.access_to_mp)
                    if(foodshift.id == 2) { // Comida
                        this.visualizeRegisterOptions("No tienes acceso en esta comida. Solo tienes derecho a desayuno y cena", View.GONE)
                    } else {
                        this.visualizeRegisterOptions("", View.VISIBLE)
                    }

                }
                this.stay.nutritional_regimen!!.code == "DE" -> {
                    if(foodshift.id > 1) { // Comida o cena
                        accessFoodsTextView.text = resources.getString(R.string.access_to_ad)
                        this.visualizeRegisterOptions("No tienes acceso en esta comida. Solo tienes derecho a desayuno",  View.GONE)
                    } else {
                        this.visualizeRegisterOptions("", View.VISIBLE)
                    }

                }
            }
            accessButton.isChecked = access
            // picnicCheckBox.visibility = View.VISIBLE
            if(this.stay.id == 2 || this.stay.id == 3) {
                access = true
                accessButton.isChecked = access
            }
            if (this.stay.count > 0) {
                clientEntryCountEditText.setText(this.stay.count.toString())
            } else {
                clientEntryCountEditText.setText("2")
            }

            addActions()


        } else {
            (activity as EntryRegisterActivity).openStartScreen()
            clientNameLastNameTextView.text = "Room in this moment not take people"
            entryDataTextView.text= "-"
            finishDataTextView.text= "-"
            clientEntryDataLinearLayout.visibility = View.GONE
            // picnicCheckBox.visibility = View.GONE
        }

    }

    fun sendDataShow(show: Boolean) {
        if (show) {
            sendDataLinearLayout.show()
            clientEntryDataLinearLayout.visibility = View.GONE
        } else {
            sendDataLinearLayout.hide()
        }
    }

    private fun addActions() {
        clientEntryCountEditText.afterTextChanged {
            // TODO Pending to add action and check if correct value in live
            //Use extension in ActionExtensions.kt
            if (it.isNotEmpty()) {
                println("Current value: " + clientEntryCountEditText.text.toString().toInt())
            }
        }
        registerEntryButton.setOnClickListener{
            var count = 1
            if (clientEntryCountEditText.text.toString() != "") {
               count = clientEntryCountEditText.text.toString().toInt()
            }
            (activity as EntryRegisterActivity).loadDataState(false)
            (activity as EntryRegisterActivity).registerClientEntry(count, 1, accessButton.text.toString())
        }
        cancelRegisterEntryButton.setOnClickListener{
            (activity as EntryRegisterActivity).openStartScreen()
        }

        unblockToRegisterButton.setOnClickListener {
            accessAutomaticSection.visibility = View.VISIBLE
            clientEntryDataLinearLayout.visibility = View.VISIBLE
            entryRegisterOptionsLinearLayout.visibility = View.VISIBLE
            unblockToRegisterButton.visibility = View.GONE
        }

    }

}