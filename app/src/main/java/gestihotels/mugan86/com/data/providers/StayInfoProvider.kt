package gestihotels.mugan86.com.data.providers

import android.support.v7.app.AppCompatActivity
import gestihotels.mugan86.com.domain.commands.RequestStayCommand
import gestihotels.mugan86.com.domain.model.Stay
import gestihotels.mugan86.com.extensions.stayItem
import gestihotels.mugan86.com.ui.interfaces.StayProvider
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 30/7/18.
 ********************************************************************************/
object StayInfoProvider: StayProvider {
    // Assign with asynchronous to use in presenter
    override fun dataStayClient(f: String, activity: AppCompatActivity, stay: stayItem) {
        doAsync {
            stayClient = dataStayClientSync(f, activity)
            uiThread {
                stay(stayClient)
            }
        }
    }

    private var stayClient : Stay = Stay()

    // return data with use synchronize
    private fun dataStayClientSync(f: String, activity: AppCompatActivity): Stay {
        Thread.sleep(500) // Half second to execute asynchronous state
        return RequestStayCommand(f, activity).execute()
    }
}