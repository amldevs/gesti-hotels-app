package gestihotels.mugan86.com.ui.fragments.main.init

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import gestihotels.mugan86.com.App
import gestihotels.mugan86.com.R
import gestihotels.mugan86.com.domain.model.FoodShift
import gestihotels.mugan86.com.ui.activities.main.MainActivity
import gestihotels.mugan86.com.ui.fragments.main.init.di.InitModule
import javax.inject.Inject

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 5/8/18.
 ********************************************************************************/
class InitFragment : Fragment(), InitPresenter.View {
    @Inject
    lateinit var presenterEntryRegister: InitPresenter
    val component by lazy { App().component.plus(InitModule(this)) }
    private var foodShift: FoodShift = FoodShift()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
        presenterEntryRegister.onCreate((activity as MainActivity))
        val b = arguments

        if (b != null) {
            foodShift = b.getParcelable("foodshift") as FoodShift
            println("FoodShift in fragment:$foodShift")
        }
    }
    // The onCreateView method is called when Fragment should create its View object hierarchy,
    // either dynamically or via XML layout inflation.
    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.fragment_entry_register_main, parent, false)
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);

    }
}