package gestihotels.mugan86.com.data.providers

import android.support.v7.app.AppCompatActivity
import gestihotels.mugan86.com.domain.commands.RequestPicnicActionCommand
import gestihotels.mugan86.com.domain.commands.RequestPicnicCommand
import gestihotels.mugan86.com.domain.model.LogPicnicAction
import gestihotels.mugan86.com.domain.model.Picnic
import gestihotels.mugan86.com.extensions.picnitItem
import gestihotels.mugan86.com.ui.interfaces.PicnicProvider
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 30/7/18.
 ********************************************************************************/
object PicnicInfoProvider: PicnicProvider {
    override fun dataPicnicItem(f: String?, activity: AppCompatActivity, picnic: picnitItem) {
        doAsync {
            picnicItem = dataPicnicItemSync(f, activity)
            uiThread {
                picnic(picnicItem)
            }
        }
    }

    private var picnicItem : Picnic = Picnic()

    // return data with use synchronize
    private fun dataPicnicItemSync(f: String?, activity: AppCompatActivity): Picnic {
        Thread.sleep(500) // Half second to execute asynchronous state
        return RequestPicnicCommand(f, activity).execute()
    }

    override fun selectPicnicAction(f: String, action: String, activity: AppCompatActivity, result: (LogPicnicAction)->Unit) {
        doAsync {
            action_ = dataPicnicActionSync(f, action, activity)
            uiThread {
                result(action_)
            }
        }
    }

    private var action_ : LogPicnicAction = LogPicnicAction()

    // return data with use synchronize
    private fun dataPicnicActionSync(f: String, action: String = "", activity: AppCompatActivity): LogPicnicAction {
        Thread.sleep(500) // Half second to execute asynchronous state
        return RequestPicnicActionCommand(f, action, activity).execute()
    }
}