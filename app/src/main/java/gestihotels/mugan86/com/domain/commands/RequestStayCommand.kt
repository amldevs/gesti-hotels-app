package gestihotels.mugan86.com.domain.commands

import gestihotels.mugan86.com.data.server.StayRequest
import gestihotels.mugan86.com.domain.model.Stay
import android.content.Context

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 29/7/18.
 ********************************************************************************/
class RequestStayCommand(private val qrCode : String = "", private val context: Context): Command<Stay> {
    override fun execute(): Stay = StayRequest(this.qrCode).execute(context = this.context)!!
}