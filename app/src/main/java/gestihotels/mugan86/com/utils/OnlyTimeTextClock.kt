package gestihotels.mugan86.com.utils

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import java.util.*

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 5/8/18.
 * Custom Text Clock to show only 24 hour time
 ********************************************************************************/
class OnlyTimeTextClock : android.widget.TextClock {
    private val DALEY = 1000.toLong()
    constructor(context: Context) : super(context) {
        updateTime()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        updateTime()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        updateTime()
    }

    private fun updateTime() {
        // First time
        setLocaleDateFormat()

        // Refresh
        Handler().postDelayed(object : Runnable {
            override fun run() {
                setLocaleDateFormat()
                if (handler != null)
                    handler.postDelayed(this, DALEY)
            }
        }, DALEY)
    }

    private fun setLocaleDateFormat() {
        // You can change language from here
        val currentLocale = Locale("es")
        val someDate = GregorianCalendar.getInstance(TimeZone.getDefault(), currentLocale)
        val dateFormat = DateTime.currentTime
        val formattedDate = dateFormat.format(someDate.time)

        this.format12Hour = formattedDate
        this.format24Hour = formattedDate
    }
}