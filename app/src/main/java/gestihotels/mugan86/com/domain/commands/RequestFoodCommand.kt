package gestihotels.mugan86.com.domain.commands

import android.content.Context
import gestihotels.mugan86.com.data.server.FoodRequest
import gestihotels.mugan86.com.domain.model.Food
import gestihotels.mugan86.com.domain.model.FoodShift
import gestihotels.mugan86.com.utils.DateTime

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 29/7/18.
 ********************************************************************************/
class RequestFoodCommand(private val typeShift : FoodShift, private val currentDate: String = DateTime.currentData,
                         val context: Context): Command<Food> {
    override fun execute(): Food = FoodRequest(this.typeShift, this.currentDate).execute(context)!!
}