package gestihotels.mugan86.com.ui.fragments.main.init.di

import dagger.Subcomponent
import gestihotels.mugan86.com.ui.fragments.main.init.InitFragment

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 5/8/18.
 ********************************************************************************/
@Subcomponent(modules = arrayOf(InitModule::class))
interface InitComponent {
    fun inject(activity: InitFragment)
}