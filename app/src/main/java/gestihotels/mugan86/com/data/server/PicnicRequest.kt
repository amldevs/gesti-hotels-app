package gestihotels.mugan86.com.data.server

import android.content.Context
import android.net.Uri
import com.google.gson.Gson
import gestihotels.mugan86.com.data.Constants
import gestihotels.mugan86.com.domain.model.LogPicnicAction
import gestihotels.mugan86.com.domain.model.Picnic
import gestihotels.mugan86.com.extensions.isNetworkConnected
import gestihotels.mugan86.com.utils.Request

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 29/7/18.
 ********************************************************************************/
class PicnicRequest(private val qrCode: String? = "", private val action: String = "") {
    fun execute(context: Context) : Picnic? {
        println(this.qrCode)
        if (isNetworkConnected(context) && this.qrCode!!.isNotEmpty()) {
            val builder = Uri.Builder()
                    // .appendQueryParameter("token", Constants.TOKEN)
                    .appendQueryParameter("id", this.qrCode)
            val request = Request().sendPost("${Constants.BASE_URL}api/food/picnic", builder, true).replace("[", "").replace("]","")
            return Gson().fromJson(request, Picnic::class.java)
        }
        //Without Internet or / and qrCode is empty
        return Picnic()
    }

    fun executeAction(context: Context): LogPicnicAction{
        println(this.action)
        if (isNetworkConnected(context) && this.qrCode!!.isNotEmpty()) {
            val builder = Uri.Builder()
                    .appendQueryParameter("token", Constants.TOKEN)
                    .appendQueryParameter("action", this.action)
                    .appendQueryParameter("id", this.qrCode)
            val request = Request().sendPost("${Constants.BASE_URL}api/picnic/manage", builder, true).replace("[", "").replace("]","")
            return Gson().fromJson(request, LogPicnicAction::class.java)
        }
        //Without Internet or / and qrCode is empty
        return LogPicnicAction()
    }
}