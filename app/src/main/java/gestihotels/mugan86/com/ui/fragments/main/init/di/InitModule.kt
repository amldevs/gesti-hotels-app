package gestihotels.mugan86.com.ui.fragments.main.init.di

import dagger.Module
import dagger.Provides
import gestihotels.mugan86.com.ui.fragments.main.init.InitFragment
import gestihotels.mugan86.com.ui.fragments.main.init.InitPresenter

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 5/8/18.
 ********************************************************************************/
@Module
class InitModule (val activity: InitFragment){
    @Provides
    fun providerInitPresenter() = InitPresenter(activity)
}
