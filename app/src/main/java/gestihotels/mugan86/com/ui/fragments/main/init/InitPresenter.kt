package gestihotels.mugan86.com.ui.fragments.main.init

import gestihotels.mugan86.com.ui.commons.Presenter

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 5/8/18.
 ********************************************************************************/
class InitPresenter(val view: View): Presenter() {
    interface View
}