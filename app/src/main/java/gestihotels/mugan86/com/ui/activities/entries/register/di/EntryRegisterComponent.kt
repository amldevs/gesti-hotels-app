package gestihotels.mugan86.com.ui.activities.entries.register.di

import dagger.Subcomponent
import gestihotels.mugan86.com.ui.activities.entries.register.EntryRegisterActivity
import gestihotels.mugan86.com.ui.activities.entries.register.di.EntryRegisterModule

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 24/7/18.
 ********************************************************************************/
@Subcomponent(modules = arrayOf(EntryRegisterModule::class))
interface EntryRegisterComponent {
    fun inject(activity: EntryRegisterActivity)
}