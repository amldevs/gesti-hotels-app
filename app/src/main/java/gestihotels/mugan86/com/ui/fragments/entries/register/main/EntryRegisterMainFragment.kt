package gestihotels.mugan86.com.ui.fragments.entries.register.main

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import gestihotels.mugan86.com.App
import gestihotels.mugan86.com.R
import gestihotels.mugan86.com.domain.model.FoodShift
import gestihotels.mugan86.com.extensions.getColorFromResource
import gestihotels.mugan86.com.extensions.getCustomStringFromDinamicallyValue
import gestihotels.mugan86.com.ui.activities.entries.register.EntryRegisterActivity
import gestihotels.mugan86.com.ui.fragments.entries.register.main.di.EntryRegisterMainModule
import kotlinx.android.synthetic.main.fragment_entry_register_main.*
import javax.inject.Inject


/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 26/7/18.
 ********************************************************************************/
class EntryRegisterMainFragment: Fragment(), EntryRegisterMainPresenter.View {
    @Inject
    lateinit var presenterEntryRegister: EntryRegisterMainPresenter
    val component by lazy { App().component.plus(EntryRegisterMainModule(this)) }
    private var foodShift: FoodShift = FoodShift()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
        presenterEntryRegister.onCreate((activity as EntryRegisterActivity))
        val b = arguments

        if (b != null) {
            foodShift = b.getParcelable("foodshift") as FoodShift
        }
    }
    // The onCreateView method is called when Fragment should create its View object hierarchy,
    // either dynamically or via XML layout inflation.
    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.fragment_entry_register_main, parent, false)
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        if (this.foodShift != FoodShift()) {
            currentFoodShift.text = "Estoy en el turno: " + getFoodShiftValue(foodShift.code!!)
            (activity as EntryRegisterActivity).setVisibilityOfScanningRegisterOptions()
        } else {
            // Closed buffet
            currentFoodShift.text = getFoodShiftValue(foodShift.code!!, "upper")
            currentFoodShift.textAlignment = View.TEXT_ALIGNMENT_CENTER
            currentFoodShift.textSize = 40.0f
            (activity as EntryRegisterActivity).setVisibilityOfScanningRegisterOptions(false)
            currentFoodShift.setTextColor(getColorFromResource(
                    (activity as EntryRegisterActivity).applicationContext,
                    R.color.Red))
        }
    }

    private fun getFoodShiftValue(code: String, type: String = ""): String {
        return getCustomStringFromDinamicallyValue(
                (activity as EntryRegisterActivity), code, type
        )
    }
}