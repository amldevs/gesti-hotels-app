package gestihotels.mugan86.com.domain.commands

import android.content.Context
import gestihotels.mugan86.com.data.server.EntryRegisterRequest
import gestihotels.mugan86.com.domain.model.Food
import gestihotels.mugan86.com.domain.model.Log
import gestihotels.mugan86.com.domain.model.Stay
import gestihotels.mugan86.com.utils.DateTime

/*****************************************************************************
 * ***
 * Created by Anartz Mugika (mugan86@gmail.com) on 2/8/18.
 ********************************************************************************/
class RequestEntryRegisterCommand(private val stay : Stay, private val currentFood: Food,
                                  private val currentDataTime: String  = DateTime.currentDataTime,
                                  private val context: Context,
                                  private val count: Int,
                                  private val picnic: Int = 0,
                                  private val access: String): Command<Log> {
    override fun execute(): Log = EntryRegisterRequest(this.stay, this.currentFood, this.currentDataTime, count, picnic, access).execute(context)!!
}