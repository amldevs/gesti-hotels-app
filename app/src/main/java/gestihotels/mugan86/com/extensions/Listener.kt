package gestihotels.mugan86.com.extensions

import gestihotels.mugan86.com.domain.model.Food
import gestihotels.mugan86.com.domain.model.Log
import gestihotels.mugan86.com.domain.model.Picnic
import gestihotels.mugan86.com.domain.model.Stay

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 30/7/18.
 ********************************************************************************/

typealias stayItem = (Stay) -> Unit

typealias foodItem = (Food) -> Unit

typealias stringItem = (String)-> Unit

typealias logItem = (Log) -> Unit

typealias picnitItem = (Picnic) -> Unit