package gestihotels.mugan86.com.ui.fragments.entries.register.main.di

import dagger.Subcomponent
import gestihotels.mugan86.com.ui.fragments.entries.register.main.EntryRegisterMainFragment

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 27/7/18.
 ********************************************************************************/
@Subcomponent(modules = arrayOf(EntryRegisterMainModule::class))
interface EntryRegisterMainComponent {
    fun inject(activity: EntryRegisterMainFragment)
}