package gestihotels.mugan86.com.ui.fragments.entries.register.main

import gestihotels.mugan86.com.ui.commons.Presenter

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 27/7/18.
 ********************************************************************************/
class EntryRegisterMainPresenter(val view: View): Presenter() {
    interface View
}