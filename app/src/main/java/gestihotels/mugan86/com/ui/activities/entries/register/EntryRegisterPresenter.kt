package gestihotels.mugan86.com.ui.activities.entries.register

import android.os.Bundle
import gestihotels.mugan86.com.domain.model.Food
import gestihotels.mugan86.com.domain.model.FoodShift
import gestihotels.mugan86.com.domain.model.Stay
import gestihotels.mugan86.com.ui.commons.Presenter
import gestihotels.mugan86.com.ui.fragments.entries.register.detail.EntryRegisterDetailFragment
import gestihotels.mugan86.com.ui.interfaces.EatEntryProvider
import gestihotels.mugan86.com.ui.interfaces.FoodProvider
import gestihotels.mugan86.com.ui.interfaces.StayProvider
import gestihotels.mugan86.com.utils.DateTime

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 24/7/18.
 ********************************************************************************/
class EntryRegisterPresenter(val view: View, private val stayProvider: StayProvider,
                             private val foodProvider: FoodProvider,
                             private val entryRegisterProvider: EatEntryProvider): Presenter() {
    interface View {
        fun showProgress()
        fun hideProgress()
        fun navigateTo(id: String)
        fun showStayDataLayout(show: Boolean)
        fun activeClientRegisterButtons()
        fun setCurrentFood(food: Food)
        fun setSelectStay( stay: Stay)
        fun setVisibilityOfScanningRegisterOptions( visible: Boolean = true)
        fun confirmDialog(message: String, title: String, log: Int)
        fun errorNotification(message: String)
    }

    private fun takeCurrentFood(foodShift: FoodShift) {
        foodProvider.dataCurrentFood(foodShift, DateTime.currentData, super.getContext()) {
            food ->
            view.setCurrentFood(food)
        }
    }

    fun takeCurrentFoodShiftInfo(): Bundle {
        val momentFoodShift = getThisMomentFoodShift()
        this.takeCurrentFood(momentFoodShift)
        val bundle = Bundle()
        bundle.putParcelable("foodshift", momentFoodShift)
        return bundle
    }

    fun checkStay(barcode: String, eat: String = "control_restaurant") {
        try {
            var takeIdValue = barcode.substring(0, barcode.indexOf("_"))
            val valuesQrCode = barcode.split("_")
            view.showStayDataLayout(false)
            view.showProgress()
            if (valuesQrCode[valuesQrCode.size  - 1] == "RM") {
                takeIdValue = "2"
            }
            if (valuesQrCode[valuesQrCode.size  - 1] == "OC") {
                takeIdValue = "3"
            }
            if(barcode.indexOf("_P_RMP") > -1) {
                view.errorNotification("El ticket que estás usando es de los Picnics")
                return
            } else {
                stayProvider.dataStayClient(takeIdValue, super.getContext()) { stayClient ->
                    view.hideProgress()
                    view.setSelectStay(stayClient)
                    val bundle = Bundle()
                    bundle.putParcelable("stay", stayClient)
                    bundle.putInt("count", stayClient.count)
                    bundle.putString("access", "Automático")
                    bundle.putInt("qrcode", takeIdValue.toInt())
                    super.openFragment(EntryRegisterDetailFragment(), "detail", bundle)
                    view.showStayDataLayout(true)
                    if (stayClient != Stay()) {
                        view.activeClientRegisterButtons()
                    }
                }
            }
        } catch (e: Exception) {
            view.errorNotification("No ha obtenido correctamente el código. Prueba de nuevo por favor")
        }
    }

    fun clientEntryRegister(stay: Stay, food: Food, count: Int, picnic: Int, access: String) {
        super.sendDataShow(true)
        entryRegisterProvider.dataEatEntryRegister(stay, DateTime.currentDataTime,
                            food, count, picnic, access, super.getContext()) {
            result ->
            println(result)
            super.sendDataShow(false)
            if (result.code == 200) {
                // Correct send data
                view.confirmDialog("Acceso PERMITIDO: \nEl cliente ${stay.client!!.name} se ha \nañadido correctamente en el registro de entradas", "Acceso permitido", result.code)
            } else if (result.code == 400) {
                // Correct but repeat register. Client not access!!!
                if (result.registerAvailable == 0) {
                    view.confirmDialog("Acceso DENEGADO: \nEl cliente ${stay.client!!.name} NO \npuede acceder al comedor por haber entrado en este turno", "Acceso DENEGADO", result.code)
                } else {
                    // if (result.stayEntryPeople!! > 1) else
                    view.confirmDialog("Acesso DENEGADO: \nEl cliente ${stay.client!!.name} ha registrado ${result.stayEntryPeople} personas \ny tiene disponible acceso a ${result.registerAvailable}", "Acceso DENEGADO", result.code)
                }
                val bundle = Bundle()
                bundle.putParcelable("stay", stay)
                bundle.putInt("count", count)
                bundle.putString("access", access)
                super.openFragment(EntryRegisterDetailFragment(), "detail", bundle)
            }


        }
    }
}