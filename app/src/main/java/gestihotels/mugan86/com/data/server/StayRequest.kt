package gestihotels.mugan86.com.data.server

import android.content.Context
import android.net.Uri
import com.google.gson.Gson
import gestihotels.mugan86.com.data.Constants
import gestihotels.mugan86.com.domain.model.Stay
import gestihotels.mugan86.com.extensions.isNetworkConnected
import gestihotels.mugan86.com.utils.Request

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 29/7/18.
 ********************************************************************************/
class StayRequest(private val qrCode: String = "") {
    fun execute(context: Context) : Stay? {
        if (isNetworkConnected(context) && !this.qrCode.isEmpty()) {
            val builder = Uri.Builder()
                    .appendQueryParameter("token", Constants.TOKEN)
                    .appendQueryParameter("qrcode", this.qrCode)
            val request = Request().sendPost("${Constants.BASE_URL}api/stay/select", builder).replace("[", "").replace("]","")
            println(request)
            return Gson().fromJson(request, Stay::class.java)
        }
        //Without Internet or / and qrCode is empty
        return Stay()
    }
}