package gestihotels.mugan86.com.data.providers

import android.support.v7.app.AppCompatActivity
import gestihotels.mugan86.com.domain.commands.RequestEntryRegisterCommand
import gestihotels.mugan86.com.domain.model.Food
import gestihotels.mugan86.com.domain.model.Log
import gestihotels.mugan86.com.domain.model.Stay
import gestihotels.mugan86.com.extensions.logItem
import gestihotels.mugan86.com.ui.interfaces.EatEntryProvider
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 2/8/18.
 ********************************************************************************/
object EatEntryRegisterProvider: EatEntryProvider {
    override fun dataEatEntryRegister(stay: Stay, datetime: String, food: Food, count: Int, picnic: Int, access: String, activity: AppCompatActivity, result: logItem) {
        doAsync {
            registerValue = dataEatEntryClientSync(stay, datetime, food, count, picnic, access, activity)
            uiThread {
                result(registerValue)
            }
        }
    }
    private var registerValue: Log = Log()
    // return data with use synchronize
    private fun dataEatEntryClientSync(client: Stay, datetime: String, food: Food, count: Int, picnic: Int, access: String, activity: AppCompatActivity): Log {
        Thread.sleep(500) // Half second to execute asynchronus state
        return RequestEntryRegisterCommand(client, food, currentDataTime = datetime, context = activity, count = count, picnic = picnic, access = access).execute()
    }
}