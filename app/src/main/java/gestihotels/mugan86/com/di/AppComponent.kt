package gestihotels.mugan86.com.di

import dagger.Component
import gestihotels.mugan86.com.App
import gestihotels.mugan86.com.ui.activities.main.di.MainComponent
import gestihotels.mugan86.com.ui.activities.main.di.MainModule
import gestihotels.mugan86.com.ui.activities.entries.register.di.EntryRegisterComponent
import gestihotels.mugan86.com.ui.activities.entries.register.di.EntryRegisterModule
import gestihotels.mugan86.com.ui.activities.picnic.check.di.PicnicCheckComponent
import gestihotels.mugan86.com.ui.activities.picnic.check.di.PicnickCheckModule
import gestihotels.mugan86.com.ui.activities.scanner.di.ScannerComponent
import gestihotels.mugan86.com.ui.activities.scanner.di.ScannerModule
import gestihotels.mugan86.com.ui.fragments.entries.register.detail.di.EntryRegisterDetailComponent
import gestihotels.mugan86.com.ui.fragments.entries.register.detail.di.EntryRegisterDetailModule
import gestihotels.mugan86.com.ui.fragments.entries.register.main.di.EntryRegisterMainComponent
import gestihotels.mugan86.com.ui.fragments.entries.register.main.di.EntryRegisterMainModule
import gestihotels.mugan86.com.ui.fragments.main.init.di.InitComponent
import gestihotels.mugan86.com.ui.fragments.main.init.di.InitModule
import javax.inject.Singleton

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 23/7/18.
 ********************************************************************************/
@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(app: App)
    // Main Activity
    fun plus(homeModule: MainModule): MainComponent
    fun plus(initModule: InitModule): InitComponent

    // Scanning QR codes
    fun plus(scannerModule: ScannerModule): ScannerComponent

    // Entry Register Activity
    fun plus(entryRegisterModule: EntryRegisterModule): EntryRegisterComponent
    fun plus(entryRegisterDetailModule: EntryRegisterDetailModule): EntryRegisterDetailComponent
    fun plus(entryRegisterMainModule: EntryRegisterMainModule): EntryRegisterMainComponent

    // Picnic Check
    fun plus(picnickCheckModule: PicnickCheckModule): PicnicCheckComponent

}