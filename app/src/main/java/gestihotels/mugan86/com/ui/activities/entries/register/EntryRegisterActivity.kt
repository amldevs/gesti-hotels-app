package gestihotels.mugan86.com.ui.activities.entries.register

import amldev.i18n.LocaleHelper
import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import gestihotels.mugan86.com.R
import gestihotels.mugan86.com.data.Constants.Companion.BAR_CODE
import gestihotels.mugan86.com.data.Constants.Companion.PRESS_AGAIN_TO_SCAN
import gestihotels.mugan86.com.data.Constants.Companion.ZXING_CAMERA_PERMISSION
import gestihotels.mugan86.com.domain.model.Food
import gestihotels.mugan86.com.domain.model.FoodShift
import gestihotels.mugan86.com.domain.model.Stay
import gestihotels.mugan86.com.extensions.app
import gestihotels.mugan86.com.extensions.hide
import gestihotels.mugan86.com.extensions.show
import gestihotels.mugan86.com.extensions.showNotification
import gestihotels.mugan86.com.ui.activities.entries.register.di.EntryRegisterModule
import gestihotels.mugan86.com.ui.activities.scanner.ScannerActivity
import gestihotels.mugan86.com.ui.fragments.entries.register.main.EntryRegisterMainFragment
import kotlinx.android.synthetic.main.content_entry_register.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import javax.inject.Inject

class EntryRegisterActivity : AppCompatActivity(), EntryRegisterPresenter.View {
    @Inject
    lateinit var presenter: EntryRegisterPresenter
    private val component by lazy { app.component.plus(EntryRegisterModule(this)) }
    private var currentBarCode = ""
    private var currentFood = Food()
    private var currentStay = Stay()

    override fun navigateTo(id: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    //To use LocaleHelper select language
    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleHelper.onAttach(base))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entry_register)
        setSupportActionBar(toolbar)
        component.inject(this)
        presenter.onCreate(this@EntryRegisterActivity)
        presenter.constructToolbar(resources.getString(R.string.app_name), backButton = true)
        this.openStartScreen()
        this.addActions()
    }

    override fun setCurrentFood(food: Food) {
        this.currentFood = food
    }

    override fun setSelectStay( stay: Stay) {
        this.currentStay = stay
    }

    private fun addActions() {
        openScanner.setOnClickListener {
            currentBarCode = "2"
            if (presenter.getThisMomentFoodShift().code != "out_food_shift") {
                launchActivity(ScannerActivity::class.java)
            } else {
                setVisibilityOfScanningRegisterOptions(false)
                val bundle = Bundle()
                bundle.putParcelable("foodshift", FoodShift())
                presenter.openFragment(EntryRegisterMainFragment(), "main", bundle)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        val barcode = data.extras.getString(BAR_CODE)

        // Toast.makeText(this@EntryRegisterActivity, barcode , Toast.LENGTH_LONG).show()

        if (barcode === "") {
            presenter.openFragment(EntryRegisterMainFragment(), "main", presenter.takeCurrentFoodShiftInfo())
            showNotification(cancelConfirmLinearLayout, resources.getString(R.string.no_take_qr_correctly), Snackbar.LENGTH_INDEFINITE, resources.getString(R.string.close_snackbar), true)
        } else {

            if (currentBarCode !== barcode) {
                currentBarCode = barcode!!
                this.loadDataState()
                presenter.checkStay(barcode)
            }

        }
    }

    fun loadDataState(start: Boolean = true) {
        if (start) {
            elementsFragment.visibility = View.GONE
            openScanner.visibility = View.GONE
        } else {
            elementsFragment.visibility = View.VISIBLE
            this.hideProgress()
        }
    }

    private fun launchActivity(clss: Class<*>) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.CAMERA), ZXING_CAMERA_PERMISSION)
        } else {
            val intent = Intent(this, clss)
            startActivityForResult(intent, 2)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            ZXING_CAMERA_PERMISSION -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! do the
                    // calendar task you need to do.
                    Toast.makeText(this, PRESS_AGAIN_TO_SCAN, Toast.LENGTH_LONG).show()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }
        }
    }
    override fun onBackPressed() {
        finish()
        return
    }
    override fun showStayDataLayout(show: Boolean) {
        if (show) elementsFragment.show()
        else elementsFragment.hide()
    }
    override fun showProgress() = progressLinearLayout.show()
    override fun hideProgress() = progressLinearLayout.hide()
    override fun activeClientRegisterButtons() {
        openScanner.hide()
    }
    override fun setVisibilityOfScanningRegisterOptions(visible: Boolean) {
        if (visible) {
            cancelConfirmLinearLayout.show()
        } else {
            cancelConfirmLinearLayout.hide()
        }
    }

    fun openStartScreen() {
        presenter.openFragment(EntryRegisterMainFragment(), "main", presenter.takeCurrentFoodShiftInfo())
        openScanner.show()
    }

    override fun confirmDialog(message: String, title: String, log: Int) {
        // custom dialog
        if (log == 200) {
            openStartScreen()
        }

        showNotification(cancelConfirmLinearLayout, message, Snackbar.LENGTH_INDEFINITE, resources.getString(R.string.close_snackbar), true)
    }

    fun registerClientEntry(count: Int = 1, picnic: Int = 0, accessManual: String = resources.getString(R.string.access_automatic)) {
        presenter.clientEntryRegister(this.currentStay, this.currentFood, count, picnic, accessManual)
    }

    override fun errorNotification(message: String) {
        showNotification(openScanner, message, Snackbar.LENGTH_INDEFINITE, resources.getString(R.string.close_snackbar), true)
        openStartScreen()
        loadDataState(false)
    }
}
