package gestihotels.mugan86.com.ui.interfaces

import android.support.v7.app.AppCompatActivity
import gestihotels.mugan86.com.domain.model.*
import gestihotels.mugan86.com.extensions.foodItem
import gestihotels.mugan86.com.extensions.logItem
import gestihotels.mugan86.com.extensions.stayItem

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 30/7/18.
 ********************************************************************************/
interface StayProvider {
    fun dataStayClient(f: String, activity: AppCompatActivity, stay: stayItem)
}

interface PicnicProvider {
    fun dataPicnicItem(f: String?, activity: AppCompatActivity, picnic: (Picnic) -> Unit)
    fun selectPicnicAction(f: String, action: String, activity: AppCompatActivity, result: (LogPicnicAction)->Unit)
}
interface  FoodProvider {
    fun dataCurrentFood(typeShift: FoodShift, data: String, activity: AppCompatActivity, food: foodItem)
}

interface  EatEntryProvider {
    fun dataEatEntryRegister(stay: Stay, datetime: String, food: Food, count: Int, picnic: Int, access: String, activity: AppCompatActivity, result: logItem)
}