package gestihotels.mugan86.com.domain.commands

import android.content.Context
import gestihotels.mugan86.com.data.server.PicnicRequest
import gestihotels.mugan86.com.domain.model.Picnic

class RequestPicnicCommand(private val qrCode : String? = "", private val context: Context): Command<Picnic> {
    override fun execute(): Picnic = PicnicRequest(this.qrCode).execute(context = this.context)!!
}