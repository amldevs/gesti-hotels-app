package gestihotels.mugan86.com.data.server

import android.content.Context
import android.net.Uri
import com.google.gson.Gson
import gestihotels.mugan86.com.data.Constants
import gestihotels.mugan86.com.domain.model.Food
import gestihotels.mugan86.com.domain.model.FoodShift
import gestihotels.mugan86.com.extensions.isNetworkConnected
import gestihotels.mugan86.com.utils.Request

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 31/7/18.
 ********************************************************************************/
class FoodRequest(private val typeFood: FoodShift, private val currentDate: String) {

    fun execute(context: Context) : Food? {
        if (isNetworkConnected(context) && typeFood.id != -1) {
            val builder = Uri.Builder()
                    .appendQueryParameter("token", Constants.TOKEN)
                    .appendQueryParameter("data", this.currentDate)
                    .appendQueryParameter("type", this.typeFood.id.toString())
            val request = Request().sendPost("${Constants.BASE_URL}api/food", builder).replace("[", "").replace("]","")
            println(request)
            return Gson().fromJson(request, Food::class.java)
        }
        //Without Internet or / and qrCode is empty
        return Food()
    }
}