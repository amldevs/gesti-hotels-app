package gestihotels.mugan86.com

import android.app.Application
import gestihotels.mugan86.com.di.AppComponent
import gestihotels.mugan86.com.di.AppModule
import gestihotels.mugan86.com.di.DaggerAppComponent

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 23/7/18.
 ********************************************************************************/
class App : Application() {

    val component: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }
}