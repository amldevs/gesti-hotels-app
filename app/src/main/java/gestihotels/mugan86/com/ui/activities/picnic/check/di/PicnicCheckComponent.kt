package gestihotels.mugan86.com.ui.activities.picnic.check.di

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 23/7/18.
 ********************************************************************************/
import dagger.Subcomponent
import gestihotels.mugan86.com.ui.activities.picnic.check.PicnickCheckActivity

@Subcomponent(modules = arrayOf(PicnickCheckModule::class))
interface PicnicCheckComponent {
    fun inject(activity: PicnickCheckActivity)
}