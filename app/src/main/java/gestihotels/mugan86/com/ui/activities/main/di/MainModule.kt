package gestihotels.mugan86.com.ui.activities.main.di

import dagger.Module
import dagger.Provides
import gestihotels.mugan86.com.ui.activities.main.MainActivity
import gestihotels.mugan86.com.ui.activities.main.MainPresenter

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 23/7/18.
 ********************************************************************************/
@Module
class MainModule(val activity: MainActivity) {
    @Provides
    fun provideMainPresenter() = MainPresenter(activity)
}