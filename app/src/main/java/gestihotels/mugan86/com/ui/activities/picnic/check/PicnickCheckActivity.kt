package gestihotels.mugan86.com.ui.activities.picnic.check

import amldev.i18n.LocaleHelper
import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import gestihotels.mugan86.com.R
import gestihotels.mugan86.com.data.Constants
import gestihotels.mugan86.com.domain.model.LogPicnicAction
import gestihotels.mugan86.com.domain.model.Picnic
import gestihotels.mugan86.com.extensions.app
import gestihotels.mugan86.com.extensions.colorize
import gestihotels.mugan86.com.extensions.showNotification
import gestihotels.mugan86.com.ui.activities.picnic.check.di.PicnickCheckModule
import gestihotels.mugan86.com.ui.activities.scanner.ScannerActivity
import gestihotels.mugan86.com.utils.DateTime
import kotlinx.android.synthetic.main.activity_picnics_check.*
import javax.inject.Inject

class PicnickCheckActivity : AppCompatActivity() , PicnickCheckPresenter.View {

    //To use LocaleHelper select language
    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleHelper.onAttach(base))
    }

    @Inject
    lateinit var presenter: PicnickCheckPresenter
    private val component by lazy { app.component.plus(PicnickCheckModule(this)) }
    private var selectPicnic: Picnic = Picnic()
    private var fromScannerOpen: String = "main"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picnics_check)
        component.inject(this)
        presenter.onCreate(this@PicnickCheckActivity)
        presenter.constructToolbar(title = resources.getString(R.string.app_name), subtitle = DateTime.currentData, backButton = true)
        picnicDataRelativeLayout.visibility = View.GONE
        this.launchActivity(ScannerActivity::class.java)
        addActions()
    }

    override fun asignPicnicInfo(picnic: Picnic) {
        this.selectPicnic = picnic
        picnicDataRelativeLayout.visibility = View.VISIBLE
        progressBarPicnics.visibility = View.GONE
        picnicClientNameTextView.text = this.selectPicnic.clientName
        picnicServiceTextView.text = this.selectPicnic.serviceName
        picnicPickUpTextView.text = this.selectPicnic.orderTime.plus(" - ").plus(this.selectPicnic.pickUpTime)
        var observations = this.selectPicnic.observations
        if (observations == "") {
            observations = resources.getString(R.string.no_observations_picnic)
        }
        picnicObservationsTextView.text = observations
        this.finishPicnicActionShow(false)

        if (picnic.total > 0) {
            picnicServiceTextView.text = picnicServiceTextView.text.toString().plus(" - ${ picnic.total }")
        }

        when {
            picnic.active == 1 -> {
                picnicStateTextView.text = resources.getString(R.string.pick_up_no_for_this_moment_picnic)
                picnicStateTextView.colorize(R.color.FacebookBackground)
            }
            picnic.active == 2 -> {
                picnicStateTextView.text = resources.getString(R.string.pick_up_ok_picnic)
                picnicStateTextView.colorize(R.color.GreenCorrectText)
                this.finishPicnicActionShow()
            }
            picnic.active == 0 -> {
                picnicStateTextView.text = resources.getString(R.string.pick_up_cancel_picnic)
                this.finishPicnicActionShow()
                picnicStateTextView.colorize(R.color.RedIncorrectText)
            }
        }
    }
    private fun launchActivity(clss: Class<*>) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.CAMERA), Constants.ZXING_CAMERA_PERMISSION)
        } else {
            val intent = Intent(this, clss)
            startActivityForResult(intent, 2)
        }
    }

    private fun finishPicnicActionShow(activeOptions: Boolean = true) {
        if(activeOptions) {
            activePicnicOptions.visibility = View.VISIBLE
            morePicnicOptions.visibility = View.VISIBLE
            cancelPicnic.visibility = View.GONE
            pickUpPicnic.visibility = View.GONE
        } else {
            activePicnicOptions.visibility = View.GONE
            morePicnicOptions.visibility = View.GONE
            cancelPicnic.visibility = View.VISIBLE
            pickUpPicnic.visibility = View.VISIBLE
        }

    }

    // /api/picnic/item
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        val barcode = data.extras.getString(Constants.BAR_CODE)

        if (barcode == "") {
            if(fromScannerOpen === "main") {
               this.onBackPressed()
            }
        } else {
            try {
                picnicStateTextView.text = barcode
                progressBarPicnics.visibility = View.VISIBLE
                picnicDataRelativeLayout.visibility = View.GONE
                presenter.getPicnic(barcode)
                // Picnic(id=216, serviceName=Desayuno, code=breakfast, clientName=Becker Magdalena, room=314, total=3, observations=, orderTime=2019-05-15, pickUpTime=07:00:00, active=1)
                picnicActions.visibility = View.VISIBLE
            } catch (e: Exception) {

            }
        }
    }

    override fun resultOperation(log: LogPicnicAction, action: String, barcode: String) {
        // picnicInfoMessagesTextView.visibility = View.VISIBLE

        val output = String.format("%s = %d", "joe", 35)
        if(log.code === 200)  {
            // picnicInfoMessagesTextView.setBackgroundColor(R.drawable.success_message_properties)
            if(action == "pickup") {
                picnicStateTextView.text = resources.getString(R.string.pick_up_ok_picnic)
                picnicStateTextView.colorize(R.color.GreenCorrectText)

                this.finishPicnicActionShow(true)
            } else if (action == "cancel") {
                picnicStateTextView.text = resources.getString(R.string.pick_up_cancel_picnic)
                picnicStateTextView.colorize(R.color.RedIncorrectText)
                this.finishPicnicActionShow(true)
            }
            // showNotification(picnicStateTextView, "Picnic correctamente ${picnicStateTextView.text.toString().toLowerCase() }", Snackbar.LENGTH_INDEFINITE, resources.getString(R.string.close_snackbar))
            showNotification(picnicStateTextView, String.format(resources.getString(R.string.picnic_mark_message_ok), picnicStateTextView.text.toString().toLowerCase()),
                    Snackbar.LENGTH_INDEFINITE, resources.getString(R.string.close_snackbar))
        } else if (log.code === 400) {
            var message = resources.getString(R.string.pick_up_ok_picnic)
            if(action == "cancel") {
               message = resources.getString(R.string.pick_up_cancel_picnic)
            }
            showNotification(picnicStateTextView, String.format(resources.getString(R.string.picnic_mark_message_error), message.toString().toLowerCase()),
                    Snackbar.LENGTH_INDEFINITE, resources.getString(R.string.close_snackbar))
        }
    }

    private fun addActions() {
        cancelPicnic.setOnClickListener {
            presenter.checkPicnicAction(this.selectPicnic.id.toString(), "cancel")
        }
        pickUpPicnic.setOnClickListener {
            presenter.checkPicnicAction(this.selectPicnic.id.toString(), "pickup")
        }
        activePicnicOptions.setOnClickListener {
            this.finishPicnicActionShow(false)
        }
        morePicnicOptions.setOnClickListener {
            fromScannerOpen = "picnic_check"
            this.launchActivity(ScannerActivity::class.java)
        }
    }
    override fun errorNotification(message: Int) {
        showNotification(imageView, resources.getString(message), Snackbar.LENGTH_INDEFINITE, resources.getString(R.string.close_snackbar), true)
        picnicActions.visibility = View.VISIBLE
        morePicnicOptions.visibility = View.VISIBLE
        activePicnicOptions.visibility = View.GONE
        cancelPicnic.visibility = View.GONE
        pickUpPicnic.visibility = View.GONE
        morePicnicOptions.text = resources.getString(R.string.manage_picnics)
        progressBarPicnics.visibility = View.GONE
        picnicDataRelativeLayout.visibility = View.VISIBLE
        picnicClientNameTextView.text = resources.getString(R.string.picnics)
        picnicServiceTextView.text = resources.getString(R.string.manage_picnics_title)
        picnicObservationsTextView.text = ""
        picnicPickUpTextView.text = ""
        picnicStateTextView.text = ""
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(0,0)
        return
    }
}
