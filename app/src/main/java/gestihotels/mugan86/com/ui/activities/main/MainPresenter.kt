package gestihotels.mugan86.com.ui.activities.main

import gestihotels.mugan86.com.ui.commons.Presenter

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 23/7/18.
 ********************************************************************************/
/**
 * Created by anartzmugika on 7/1/18.
 */
class MainPresenter(val view: View): Presenter() {

    interface View
}