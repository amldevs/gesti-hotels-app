package gestihotels.mugan86.com.extensions

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 29/9/18.
 ********************************************************************************/
fun EditText.afterTextChanged(afterTextChanged: StringUnit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun afterTextChanged(editable: Editable?) = afterTextChanged.invoke(editable.toString())
    })
}

typealias StringUnit = (String) -> Unit