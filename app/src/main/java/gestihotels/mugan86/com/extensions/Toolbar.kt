package gestihotels.mugan86.com.extensions

import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import gestihotels.mugan86.com.R
import kotlinx.android.synthetic.main.toolbar_layout.*

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 13/1/18.
 ********************************************************************************/
fun AppCompatActivity.addToolbar(color: Int = ContextCompat.getColor(this, R.color.White),
                                 title: String = resources.getString(R.string.app_name), subtitle: String = "" ,
                                 navigationIcon: Int = R.drawable.ic_back, backButton: Boolean = false) {
    supportActionBar?.setDisplayHomeAsUpEnabled(false)
    toolbar.title = title
    toolbar.setTitleTextColor(color)
    if (subtitle != "") toolbar.subtitle = subtitle
    setSupportActionBar(toolbar)

    if (backButton) {
        toolbar.navigationIcon = getDrawableFromResource(this, navigationIcon)
        toolbar.setNavigationOnClickListener {
            // What to do on back clicked
            finish()
        }
    }
}