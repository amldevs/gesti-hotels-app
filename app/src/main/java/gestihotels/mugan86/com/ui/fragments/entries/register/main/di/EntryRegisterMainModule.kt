package gestihotels.mugan86.com.ui.fragments.entries.register.main.di

import dagger.Module
import dagger.Provides
import gestihotels.mugan86.com.ui.fragments.entries.register.main.EntryRegisterMainFragment
import gestihotels.mugan86.com.ui.fragments.entries.register.main.EntryRegisterMainPresenter

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 27/7/18.
 ********************************************************************************/
@Module
class EntryRegisterMainModule(val activity: EntryRegisterMainFragment) {
    @Provides
    fun provideEntryRegisterMainPresenter() = EntryRegisterMainPresenter(activity)
}