package gestihotels.mugan86.com.domain.commands

import android.content.Context
import gestihotels.mugan86.com.data.server.PicnicRequest
import gestihotels.mugan86.com.domain.model.LogPicnicAction
import gestihotels.mugan86.com.domain.model.Picnic

class RequestPicnicActionCommand(private val qrCode : String = "", private val action: String = "", private val context: Context): Command<LogPicnicAction> {
    override fun execute(): LogPicnicAction = PicnicRequest(this.qrCode, this.action).executeAction(context = this.context)!!
}