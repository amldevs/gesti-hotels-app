package gestihotels.mugan86.com.di

import dagger.Module
import dagger.Provides
import gestihotels.mugan86.com.App
import gestihotels.mugan86.com.data.providers.EatEntryRegisterProvider
import gestihotels.mugan86.com.data.providers.FoodInfoProvider
import gestihotels.mugan86.com.data.providers.PicnicInfoProvider
import gestihotels.mugan86.com.data.providers.StayInfoProvider
import gestihotels.mugan86.com.ui.interfaces.EatEntryProvider
import gestihotels.mugan86.com.ui.interfaces.FoodProvider
import gestihotels.mugan86.com.ui.interfaces.PicnicProvider
import gestihotels.mugan86.com.ui.interfaces.StayProvider
import javax.inject.Singleton

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 23/7/18.
 ********************************************************************************/
@Module
class AppModule(val app: App) {
    @Provides
    @Singleton
    fun provideApp() = app
    @Provides
    @Singleton
    fun provideEntryRegisterProvider(): StayProvider = StayInfoProvider
    @Provides
    @Singleton
    fun provideFoodProvider(): FoodProvider = FoodInfoProvider
    @Provides
    @Singleton
    fun provideEatRegisterProvider(): EatEntryProvider = EatEntryRegisterProvider
    @Provides
    @Singleton
    fun providePicnicInfoProvider(): PicnicProvider = PicnicInfoProvider
}