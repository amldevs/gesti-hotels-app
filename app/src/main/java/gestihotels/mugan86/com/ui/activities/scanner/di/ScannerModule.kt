package gestihotels.mugan86.com.ui.activities.scanner.di

import dagger.Module
import dagger.Provides
import gestihotels.mugan86.com.ui.activities.scanner.ScannerActivity
import gestihotels.mugan86.com.ui.activities.scanner.ScannerPresenter

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 23/7/18.
 ********************************************************************************/
@Module
class ScannerModule(val activity: ScannerActivity) {
    @Provides
    fun provideMainPresenter() = ScannerPresenter(activity)
}