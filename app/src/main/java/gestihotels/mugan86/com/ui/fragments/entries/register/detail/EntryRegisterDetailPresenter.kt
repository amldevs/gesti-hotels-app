package gestihotels.mugan86.com.ui.fragments.entries.register.detail

import gestihotels.mugan86.com.ui.commons.Presenter

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 27/7/18.
 ********************************************************************************/
class EntryRegisterDetailPresenter(val view: View): Presenter() {
    interface View
}