package gestihotels.mugan86.com.extensions

import android.app.Activity
import android.app.Dialog
import android.os.Build
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.view.*
import android.widget.TextView


/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 30/7/18.
 ********************************************************************************/
/**
 * Created by anartzmugika on 1/1/18.
 */
fun ViewGroup.inflate(resource: Int): View {
    return LayoutInflater.from(context).inflate(resource, this, false)
}

inline fun <reified T: View> View.find(id: Int): T {
    return findViewById<T>(id)
}

fun View.show() = run { visibility = View.VISIBLE }

fun View.hide() = run { visibility = View.GONE }


fun customDialog(activity: Activity, layout: Int): Dialog {
    val dialog = Dialog(activity)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.setContentView(layout)
    val lp = WindowManager.LayoutParams()
    lp.copyFrom(dialog.window!!.attributes)
    lp.width = WindowManager.LayoutParams.WRAP_CONTENT
    lp.height = WindowManager.LayoutParams.WRAP_CONTENT
    dialog.window!!.attributes = lp
    return dialog
}

/**
 * Function to paint select textview in select color
 */
fun TextView.colorize(color: Int) = run {
    if(Build.VERSION.SDK_INT < 23) {
        this.setTextColor(resources.getColor(color))
    } else {
        this.setTextColor(ContextCompat.getColor(context, color))
    }
}

/**
 * v: View => Add view layout
 * text: String => Message content
 * time: Int => Indefine, Long or short
 * actionText: String => Close notification text
 */
fun showNotification(v: View, text: String, time: Int, actionText: String, multiline: Boolean = false) {
    if(!multiline) {
        Snackbar.make(v, text, time)
                .setAction(actionText) { }
                .show()
        return
    }
    val snackbar = Snackbar.make(v, text, time)
    val textView = snackbar.view.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
    textView.setSingleLine(false)
    textView.maxLines = 6
    snackbar.setAction(actionText) { }
    snackbar.show()
}