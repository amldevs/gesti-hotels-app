package gestihotels.mugan86.com.ui.activities.picnic.check

import gestihotels.mugan86.com.R
import gestihotels.mugan86.com.domain.model.LogPicnicAction
import gestihotels.mugan86.com.domain.model.Picnic
import gestihotels.mugan86.com.ui.commons.Presenter
import gestihotels.mugan86.com.ui.interfaces.PicnicProvider

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 23/7/18.
 ********************************************************************************/
/**
 * Created by anartzmugika on 7/1/18.
 */
class PicnickCheckPresenter(val view: View, private val picnicProvider: PicnicProvider): Presenter() {

    interface View {
        fun asignPicnicInfo(picnic: Picnic)
        fun resultOperation(log: LogPicnicAction, action: String, barcode: String)
        fun errorNotification(message: Int)
    }

    fun getPicnic(barcode: String?) {
        try {
            val takeIdValue = barcode?.substring(0, barcode.indexOf("_"))
            println(takeIdValue)
            println(barcode!!.indexOf("_P_RMP") )
            if(barcode.indexOf("_P_RMP") > -1) {
                picnicProvider.dataPicnicItem(takeIdValue, super.getContext()) {
                    item ->
                    println(item)
                    view.asignPicnicInfo(item)
                }
            } else {
                view.errorNotification(R.string.code_not_correct_picnic_qr_code)
            }
        } catch (e: Exception) {
            view.errorNotification(R.string.unexpect_error_try_again_please)
        }


    }

    fun checkPicnicAction(barcode: String, action: String) {
        picnicProvider.selectPicnicAction(barcode, action, super.getContext()) {
            item ->
            view.resultOperation(item, action, barcode)
        }
    }
}