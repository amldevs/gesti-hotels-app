package gestihotels.mugan86.com.utils

import android.net.Uri
import java.io.BufferedReader
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 25/7/18.
 ********************************************************************************/
class Request {

    private val USER_AGENT = "Mozilla/5.0"

    // HTTP POST request
    @Throws(Exception::class)
    fun sendPost(url: String, builder: Uri.Builder , debug: Boolean = false): String {

        val obj = URL(url)
        val con = obj.openConnection() as HttpURLConnection
        var reader: BufferedReader? = null
        var stringBuilder: StringBuilder

        //add request header
        con.requestMethod = "POST"
        con.setRequestProperty("User-Agent", USER_AGENT)
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5")

        //Encode Query parameters....
        val query = builder.build().encodedQuery


        // Send post request
        con.doOutput = true
        val wr = DataOutputStream(con.getOutputStream())
        wr.writeBytes(query)
        wr.flush()
        wr.close()

        val responseCode = con.responseCode
        if (debug) {
            println("\nSending 'POST' request to URL : $url")
            println("Post parameters : $query")
            println("Response Code : $responseCode")
        }

        reader = BufferedReader(InputStreamReader(con.inputStream))
        val response = StringBuffer()
        var line: String? = null
        stringBuilder = StringBuilder()

        val reader_ = BufferedReader(reader)
        while ({ line = reader_.readLine(); line }() != null) {
            if (debug) System.out.println(line);
            stringBuilder = stringBuilder.append(line)
        }

        reader.close()
        con.disconnect()

        if (debug) println(response.toString())
        return stringBuilder.toString()

    }

}