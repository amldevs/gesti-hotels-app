package gestihotels.mugan86.com.ui.activities.picnic.check.di

import dagger.Module
import dagger.Provides
import gestihotels.mugan86.com.ui.activities.picnic.check.PicnickCheckPresenter
import gestihotels.mugan86.com.ui.activities.picnic.check.PicnickCheckActivity
import gestihotels.mugan86.com.ui.interfaces.PicnicProvider

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 23/7/18.
 ********************************************************************************/
@Module
class PicnickCheckModule(val activity: PicnickCheckActivity) {
    @Provides
    fun providePicnicCheckPresenter(picnicProvider: PicnicProvider) = PicnickCheckPresenter(activity, picnicProvider)
}