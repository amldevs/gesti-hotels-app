package gestihotels.mugan86.com.ui.activities.scanner

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 24/7/18.
 ********************************************************************************/
/**
 * Created by anartzmugika on 7/1/18.
 */
class ScannerPresenter(val view: View) {

    interface View {
        fun showProgress()
        fun hideProgress()
        fun navigateTo(id: String)
    }

}