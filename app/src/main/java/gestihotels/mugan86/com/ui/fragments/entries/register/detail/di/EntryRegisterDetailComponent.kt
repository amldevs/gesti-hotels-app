package gestihotels.mugan86.com.ui.fragments.entries.register.detail.di

import dagger.Subcomponent
import gestihotels.mugan86.com.ui.fragments.entries.register.detail.EntryRegisterDetailFragment

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 27/7/18.
 ********************************************************************************/
@Subcomponent(modules = arrayOf(EntryRegisterDetailModule::class))
interface EntryRegisterDetailComponent {
    fun inject(activity: EntryRegisterDetailFragment)
}