package gestihotels.mugan86.com.data.server

import android.content.Context
import com.google.gson.Gson
import gestihotels.mugan86.com.data.Constants
import gestihotels.mugan86.com.domain.model.Hotel
import gestihotels.mugan86.com.extensions.isNetworkConnected
import java.net.URL

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 29/7/18.
 ********************************************************************************/
class HotelRequest(val id: Int = -1){
    fun execute(context: Context) : Hotel? {
        if (isNetworkConnected(context) && this.id > 0) {
            // if (this.id == -1)  return Gson().fromJson(URL("${Constants.BASE_URL}api/hotels").readText(), HotelRequest::class.java)
            return Gson().fromJson(URL("${Constants.BASE_URL}api/hotels/${this.id}").readText(), Hotel::class.java)
        }
        //Without Internet
        return null
    }
}