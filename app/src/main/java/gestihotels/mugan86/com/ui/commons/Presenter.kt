package gestihotels.mugan86.com.ui.commons

import amldev.i18n.LocaleHelper
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import gestihotels.mugan86.com.R
import gestihotels.mugan86.com.data.ActivityReferences
import gestihotels.mugan86.com.domain.model.FoodShift
import gestihotels.mugan86.com.extensions.addToolbar
import gestihotels.mugan86.com.extensions.getJSONResource
import gestihotels.mugan86.com.extensions.navigate
import gestihotels.mugan86.com.ui.activities.entries.register.EntryRegisterActivity
import gestihotels.mugan86.com.ui.activities.main.MainActivity
import gestihotels.mugan86.com.ui.activities.picnic.check.PicnickCheckActivity
import gestihotels.mugan86.com.ui.fragments.entries.register.detail.EntryRegisterDetailFragment
import gestihotels.mugan86.com.utils.DateTime

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 24/7/18.
 ********************************************************************************/
abstract class Presenter {
    private lateinit var context: AppCompatActivity
    private lateinit var fragmentValue: Fragment
    open fun onCreate(context: AppCompatActivity) {
        this.context = context
    }
    open fun navigateTo(activityReferences: ActivityReferences) {
        if(activityReferences == ActivityReferences.ENTRYREGISTER) {
            context.navigate<EntryRegisterActivity>(finishThisActivity = false)
        }
        if(activityReferences == ActivityReferences.MAINACTIVITY) {
            context.navigate<MainActivity>()
        }
        if(activityReferences == ActivityReferences.PICNICCHECKS) {
            context.navigate<PicnickCheckActivity>(finishThisActivity = false)
        }
    }
    fun changeLang(code: String) {
        LocaleHelper.changeLang(this.context, code)
        println("Change language to ${code}")
    }

    fun constructToolbar(title: String, subtitle: String = DateTime.currentData, backButton: Boolean = false) {
        (context).addToolbar(title = title, subtitle = subtitle, backButton = backButton)
    }
    fun getContext() = this.context

    fun openFragment(fragment: Fragment, tag: String, bundle: Bundle?) {

        if (bundle != null) {
            fragmentValue = fragment
            fragment.arguments = bundle
        } else {
            fragmentValue = fragment
        }

        context.supportFragmentManager.beginTransaction()
                .replace(R.id.elementsFragment, fragmentValue, tag)
                .addToBackStack(tag)
                .commitAllowingStateLoss()
    }

    fun sendDataShow(show: Boolean) {
        (this.fragmentValue as EntryRegisterDetailFragment).sendDataShow(show)
    }

    private fun loadFoodShiftsData(): ArrayList<FoodShift> {
        val shifts: ArrayList<FoodShift> = ArrayList()
        ((Parser().parse(StringBuilder(getJSONResource(this.context, "food-shifts")))
                as JsonArray<*>).map {
            shift ->
            val data = shift as JsonObject
            shifts.add(FoodShift(Integer.parseInt(data["id"].toString()), data["code"].toString(),
                    data["name"].toString(), data["start"].toString(), data["finish"].toString()))
        })
        return shifts
    }

    fun getThisMomentFoodShift(addHours: Int = -1): FoodShift {
        val currentHour = if (addHours == -1 ) {
            Integer.parseInt(DateTime.currentHour)
            // val currentHour = 7;
        } else {
            Integer.parseInt(DateTime.currentHour) + addHours
        }
        var foodShift = FoodShift()
        loadFoodShiftsData().map {
            shift ->
            val shiftStartHour = Integer.parseInt(shift.start!!.substring(0, 2))
            val shiftFinishHour = Integer.parseInt(shift.finish!!.substring(0, 2))
            if(currentHour in shiftStartHour..(shiftFinishHour - 1)) {
                foodShift = shift
            }
            if (currentHour == shiftFinishHour) {
                val shiftFinishMinute = Integer.parseInt(shift.finish!!.substring(3, 5))
                println(shiftFinishMinute)
                if (DateTime.currentMinute.toInt() <= shiftFinishMinute) {
                    foodShift = shift
                }
            }
        }
        return foodShift
    }

    interface View {

        fun context(): Context
    }
}