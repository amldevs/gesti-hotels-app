package gestihotels.mugan86.com.ui.activities.entries.register.di

import dagger.Module
import dagger.Provides
import gestihotels.mugan86.com.ui.activities.entries.register.EntryRegisterActivity
import gestihotels.mugan86.com.ui.activities.entries.register.EntryRegisterPresenter
import gestihotels.mugan86.com.ui.interfaces.EatEntryProvider
import gestihotels.mugan86.com.ui.interfaces.FoodProvider
import gestihotels.mugan86.com.ui.interfaces.StayProvider

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 24/7/18.
 ********************************************************************************/
@Module
class EntryRegisterModule(val activity: EntryRegisterActivity) {
    @Provides
    fun provideEntryRegisterPresenter(stayProvider: StayProvider, foodProvider: FoodProvider,
                                      eatEntryRegisterProvider: EatEntryProvider) =
            EntryRegisterPresenter(activity, stayProvider, foodProvider, eatEntryRegisterProvider)
}