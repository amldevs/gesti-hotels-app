package gestihotels.mugan86.com.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.google.gson.JsonParser
import gestihotels.mugan86.com.App
import java.io.InputStreamReader


/***************************************************************************************************************
 * ¿Alguna vez has intentado utilizar la clase de un tipo genérico dentro de una función? Debido al type erasure
 * (borrado de tipos) que realiza la JVM cuando compila, esto es imposible. Por eso, habrás visto (o incluso usado)
 * muchas veces que se pasa un .class como parámetro de una función.
 *
 * Gracias a las funciones reified, esto va a cambiar.
 *****************************************************************************************************************/
inline fun <reified T: Activity> Context.startActivity() {
    startActivity(Intent(this, T::class.java))
}

inline fun <reified T : Activity> Activity.navigate(id: String = "", finishThisActivity: Boolean = true) {
    val intent = Intent(this, T::class.java)
    if (id == "") intent.putExtra("id", id)
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
    startActivity(intent)
    if (finishThisActivity) finish()
    overridePendingTransition(0,0)
}

val Activity.app: App
    get() = application as App


fun isNetworkConnected(context: Context): Boolean {
    /**************************************************************************************************
     * Retrieving an instance of the ConnectivityManager class from the current application context.
     * The ConnectivityManager class is simply your go to class if you need any information about the
     * state of network connectivity. It can also be set up to report network connection
     * changes to your application.
     **************************************************************************************************/
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    /**************************************************************************************************
     * Retrieving information about the current active network connection.
     * This will be null if there's no network connection.
     **************************************************************************************************/
    val networkInfo = connectivityManager.activeNetworkInfo // 2

    /**************************************************************************************************
     * Checking if the device is connected to an available network connection.
     **************************************************************************************************/
    return networkInfo != null && networkInfo.isConnected // 3
}

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 6/8/17.
 ********************************************************************************/
fun showHideKeyBoardForce(editText: EditText, show: Boolean, context: Context) {
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    if (show) {
        editText.requestFocus()
        editText.selectAll()
        inputMethodManager.toggleSoftInputFromWindow(editText.applicationWindowToken, InputMethodManager.SHOW_FORCED, 0)
    } else inputMethodManager.toggleSoftInputFromWindow(editText.applicationWindowToken, InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
}

@RequiresApi(api = Build.VERSION_CODES.KITKAT)
fun getJSONResource(context: Context, name: String): String? {
    try {
        context.assets.open("$name.json").use { `is` ->
            val parser = JsonParser()
            return parser.parse(InputStreamReader(`is`)).toString()
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return null
}

fun getDrawableResource(context: Context, resource: String) =
        context.resources.getIdentifier("ic_$resource", "drawable", context.packageName)

fun getCustomStringFromDinamicallyValue(context: Context, string: String, type: String): String {
    val id: Int = context.resources.getIdentifier(string, "string", context.packageName)
    if (type == "upper") {
        return context.getString(id).toUpperCase()
    } else if (type == "lower"){
        return context.getString(id).toLowerCase()
    }
    return context.getString(id)
}

fun getColorFromResource(context: Context, color: Int) =
        ContextCompat.getColor(context, color)

fun getDrawableFromResource(context: Context, drawable: Int) =
        ContextCompat.getDrawable(context, drawable)


