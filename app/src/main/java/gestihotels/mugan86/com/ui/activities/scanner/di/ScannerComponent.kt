package gestihotels.mugan86.com.ui.activities.scanner.di

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 23/7/18.
 ********************************************************************************/
import dagger.Subcomponent
import gestihotels.mugan86.com.ui.activities.scanner.ScannerActivity

@Subcomponent(modules = arrayOf(ScannerModule::class))
interface ScannerComponent {
    fun inject(activity: ScannerActivity)
}