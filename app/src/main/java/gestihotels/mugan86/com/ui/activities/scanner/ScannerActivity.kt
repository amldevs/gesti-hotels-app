package gestihotels.mugan86.com.ui.activities.scanner

import amldev.i18n.LocaleHelper
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import com.google.zxing.Result
import gestihotels.mugan86.com.data.Constants
import gestihotels.mugan86.com.extensions.app
import gestihotels.mugan86.com.ui.activities.scanner.di.ScannerModule
import me.dm7.barcodescanner.zxing.ZXingScannerView

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 24/7/18.
 ********************************************************************************/
class ScannerActivity : Activity(), ScannerPresenter.View, ZXingScannerView.ResultHandler {
    private val component by lazy { app.component.plus(ScannerModule(this)) }
    override fun showProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun navigateTo(id: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    //To use LocaleHelper select language
    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleHelper.onAttach(base))
    }

    private var mScannerView: ZXingScannerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)

        mScannerView = ZXingScannerView(this)   // Programmatically initialize the scanner view
        setContentView(mScannerView)

        mScannerView!!.setResultHandler(this) // Register ourselves as a handler for scan results.
        mScannerView!!.startCamera()         // Start camera


    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    override fun onPause() {
        super.onPause()
        this.cleanResult()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    override fun onBackPressed() {
        this.cleanResult()
    }

    private fun cleanResult() {
        try {
            mScannerView!!.stopCamera() // Stop camera on pause
        } catch (e: Exception) {
            Log.e(Constants.ERROR, e.message)
        }

        val resultIntent = Intent()
        resultIntent.putExtra(Constants.BAR_CODE, "")
        setResult(2, resultIntent)
        finish()
    }

    override fun handleResult(rawResult: Result) {
        // Do something with the result here

        Log.e(Constants.HANDLER, rawResult.text) // Prints scan results
        Log.e(Constants.HANDLER, rawResult.barcodeFormat.toString()) // Prints the scan format (qrcode)

        try {
            mScannerView!!.stopCamera()

            // Add Sound to comfirm correct take sound
            MediaPlayer.create(this, Constants.SCAN_OK_SOUND).start()
            val resultIntent = Intent()

            println(rawResult.text)
            resultIntent.putExtra(Constants.BAR_CODE, rawResult.text)
            setResult(2, resultIntent)
            finish()

            println(Constants.STOP_CAMERA)
            // Stop camera on pause
        } catch (e: Exception) {
            Log.e(Constants.ERROR, e.message)
            MediaPlayer.create(this, Constants.SCAN_ERROR_SOUND).start()
        }

    }
}
