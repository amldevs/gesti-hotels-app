package gestihotels.mugan86.com.ui.activities.main

import amldev.i18n.LocaleHelper
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import gestihotels.mugan86.com.R
import gestihotels.mugan86.com.data.ActivityReferences
import gestihotels.mugan86.com.extensions.app
import gestihotels.mugan86.com.ui.activities.main.di.MainModule
import gestihotels.mugan86.com.utils.DateTime
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() , MainPresenter.View {

    //To use LocaleHelper select language
    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleHelper.onAttach(base))
    }

    @Inject
    lateinit var presenter: MainPresenter
    private val component by lazy { app.component.plus(MainModule(this)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        component.inject(this)
        presenter.onCreate(this@MainActivity)
        presenter.constructToolbar(title = resources.getString(R.string.app_name), subtitle = DateTime.currentData)
        addActions()
    }

    private fun addActions() {
        goToPicnicCheck.setOnClickListener{ presenter.navigateTo(ActivityReferences.PICNICCHECKS)}
        goToRegisterEntryButton.setOnClickListener { presenter.navigateTo(ActivityReferences.ENTRYREGISTER) }
    }
}
