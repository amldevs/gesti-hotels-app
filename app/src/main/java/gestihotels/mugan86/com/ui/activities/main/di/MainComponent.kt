package gestihotels.mugan86.com.ui.activities.main.di

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 23/7/18.
 ********************************************************************************/
import dagger.Subcomponent
import gestihotels.mugan86.com.ui.activities.main.MainActivity

@Subcomponent(modules = arrayOf(MainModule::class))
interface MainComponent {
    fun inject(activity: MainActivity)
}