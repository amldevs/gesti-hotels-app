package gestihotels.mugan86.com.data.providers

import android.support.v7.app.AppCompatActivity
import gestihotels.mugan86.com.domain.commands.RequestFoodCommand
import gestihotels.mugan86.com.domain.model.Food
import gestihotels.mugan86.com.domain.model.FoodShift
import gestihotels.mugan86.com.extensions.foodItem
import gestihotels.mugan86.com.ui.interfaces.FoodProvider
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/********************************************************************************
 * Created by Anartz Mugika (mugan86@gmail.com) on 30/7/18.
 ********************************************************************************/
object FoodInfoProvider: FoodProvider {
    override fun dataCurrentFood(typeShift: FoodShift, data: String, activity: AppCompatActivity, food: foodItem) {
        doAsync {
            currentFood = dataStayClientSync(typeShift, data, activity)
            uiThread {
                food(currentFood)
            }
        }
    }
    private var currentFood: Food = Food()
    // return data with use synchronize
    private fun dataStayClientSync(typeShift: FoodShift, data: String, activity: AppCompatActivity): Food {
        Thread.sleep(500) // Half second to execute asynchronus state
        return RequestFoodCommand(typeShift, data, activity).execute()
    }
}